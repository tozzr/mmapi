package com.tozzr.mmapi;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.util.List;

import org.junit.Test;

public class ParserTest {

	@Test
	public void test() throws Exception {
		Parser p = new Parser();
		List<Animal> animals = p.parse(new File("src/test/resources/search.html"));
		
		assertThat(animals.size(), equalTo(13));
		assertThat(animals.get(0).getTitle(), equalTo("Ghost Ball Python"));
		assertThat(animals.get(0).getId(), equalTo(239060));
		assertThat(animals.get(0).getPrice(), equalTo(75.0));
		assertThat(animals.get(0).getStore(), equalTo("UGS Berlin"));
		assertThat(animals.get(0).getSex(), equalTo("female"));
		assertThat(animals.get(0).getDob(), equalTo("19"));
		assertThat(animals.get(0).getLinks().get("self"), equalTo("https://morphmarket.com/eu/c/reptiles/pythons/ball-pythons/239060"));
		assertThat(animals.get(0).getLinks().get("img"), equalTo("https://morphmarket-media.s3.amazonaws.com/media/cache/e1/2e/e12e74d88ccf85b5bdbccd0e31a6272d.jpg"));
	}
}
