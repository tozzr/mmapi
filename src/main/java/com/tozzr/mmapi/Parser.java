package com.tozzr.mmapi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

@Component
class Parser {

	public List<Animal> parse(File file) throws IOException {
		Document doc = Jsoup.parse(file, "UTF-8");
		return parseDoc(doc);
	}

	public List<Animal> parse(String url) throws IOException {
		Document doc = Jsoup.connect(url).get();
		return parseDoc(doc);
	}

	private List<Animal> parseDoc(Document doc) {
		List<Animal> animals = new ArrayList<>();
		doc.select(".item-col a").forEach(s -> {
			animals.add(
				new Animal(
					Integer.valueOf(s.attr("href").replace("/eu/c/reptiles/pythons/ball-pythons/", "")),
					s.attr("title"),
					"https://morphmarket.com" + s.attr("href"),
					Double.valueOf(s.select(".price").text().replace("€", "")),
					s.select(".store").text(),
					s.select(".sex").attr("alt"),
					s.select(".dob").text().replace("'", ""),
					s.select(".snake-thumb").attr("src")
				)
			);
		});
		return animals;
	}
}