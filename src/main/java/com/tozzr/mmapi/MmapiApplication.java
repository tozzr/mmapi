package com.tozzr.mmapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmapiApplication.class, args);
	}

}
