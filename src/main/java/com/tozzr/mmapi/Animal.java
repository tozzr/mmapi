package com.tozzr.mmapi;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

class Animal {

	private String title;
	private int id;
	private double price;
	private String store;
	private String sex;
	private String dob;
	@JsonProperty("_links")
	private Map<String, String> links;

	public Animal(int id, String title, String url, double price, String store, String sex, String dob, String img) {
		this.id = id;
		this.title = title;
		this.sex = sex;
		this.dob = dob;
		this.price = price;
		this.store = store;
		this.sex = sex;
		this.dob = dob;
		this.links = new LinkedHashMap<>();
		this.links.put("self", url);
		this.links.put("img", img);
	}

	public String getTitle() {
		return title;
	}

	public int getId() {
		return id;
	}

	public double getPrice() {
		return price;
	}

	public String getStore() {
		return store;
	}

	public String getSex() {
		return sex;
	}

	public String getDob() {
		return dob;
	}
	
	public Map<String, String> getLinks() {
		return links;
	}
	
}