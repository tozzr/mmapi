package com.tozzr.mmapi;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	private Parser parser;
	
	public MainController(Parser parser) {
		this.parser = parser;
	}

	@GetMapping("/")
	public List<Animal> getIndex() throws Exception {
		return parse();
	}

	private List<Animal> parse() throws Exception {
		String url = "https://www.morphmarket.com/eu/search?"
				+ "q=&sex=f&maturity=0&cat=2&min_genes=0&max_genes=9"
				+ "&traits=Ghost&neg_traits=&min_price=0&max_price=1000000"
				+ "&cur=EUR&sort=le&epoch=0&store=&country=DE&export=&layout=grid"; 
//		return parser.parse(url);
		return parser.parse(new File("src/test/resources/search.html"));
	}
	
}
